﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;

namespace Otus.Teaching.PromoCodeFactory.SignalRClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var connection = new HubConnectionBuilder()
                .WithUrl("https://localhost:5001/hubs/customer")
                .WithAutomaticReconnect()
                .Build();

            await connection.StartAsync();

            connection.On("ReceiveCustomerUpdate", (string id) =>
            {
                // show alert to the user
                Console.WriteLine($"Updated customer with id: {id}");
            });


            var createResponse = await connection.InvokeAsync<CustomerResponse>("CreateCustomerAsync",
                new CreateOrEditCustomerRequest()
                {
                    FirstName = "Name",
                    LastName = "lastName",
                    Email = "email",
                    PreferenceIds = new List<Guid>()
                    {
                        Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84")
                    }
                });
            
            await connection.InvokeAsync<string>("SubscribeOnCustomer", createResponse.Id.ToString());

            var editResponse = await connection.InvokeAsync<string>("EditCustomersAsync",
                createResponse.Id,
                new CreateOrEditCustomerRequest()
                {
                    FirstName = "NewName",
                    LastName = "newLastName",
                    PreferenceIds = new List<Guid>()
                    {
                        Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84")
                    }
                });
            
            Console.ReadLine();
            await connection.StopAsync();
        }
    }
}