﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromocodeFactory.WebHost;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Empty = Google.Protobuf.WellKnownTypes.Empty;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Grpc
{
    public class GrpcCustomerController: CustomerService.CustomerServiceBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<CustomerPreference> _customerPreferenceRepository;
        public GrpcCustomerController(
            IRepository<Customer> customerRepository, 
            IRepository<Preference> preferenceRepository, 
            IRepository<CustomerPreference> customerPreferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _customerPreferenceRepository = customerPreferenceRepository;
        }
        
        public override async Task<CustomerShortReplyCollection> GetCustomers(Empty request, ServerCallContext context)
        {
            var customers =  await _customerRepository.GetAllAsync();

            var response = customers.Select(x => new CustomerShortReply()
            {
                Id = x.Id.ToString(),
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();
            
            var responceCollectins = new CustomerShortReplyCollection();
            responceCollectins.Reply.Add(response);
            
            return responceCollectins;
        }

        public override async Task<CustomerReply> GetCustomerById(GuidRequest request, ServerCallContext context)
        {
            var customer =  await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));

            var response = CustomerMapper.ToCustomerReply(customer);

            return response;
        }

        public override async Task<CustomerReply> CreateCustomer(CreateCustomerRequest request, ServerCallContext context)
        {
            //Получаем предпочтения из бд и сохраняем большой объект
            var preferences = await _preferenceRepository
                .GetRangeByIdsAsync(request.PreferenceIds.Select(x=>Guid.Parse(x)).ToList());

           // var 
            Customer customer = CustomerMapper.MapFromCreateRequest(request, preferences);
            
            await _customerRepository.AddAsync(customer);

            return CustomerMapper.ToCustomerReply(customer);
        }

        public override async Task<StatusCodeReply> EditCustomer(EditCustomerRequest request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));
            
            if (customer == null)
                return new StatusCodeReply()
                {
                    Status = StatusCodes.Status404NotFound
                };
            
            var preferences = 
                await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds.Select(Guid.Parse).ToList());
            
            //удаляем старые предпочтения пользователя
            await _customerPreferenceRepository.DeleteRangeAsync(
                customer.Preferences);
            
            CustomerMapper.MapFromEditRequest(request, preferences, customer);

            await _customerRepository.UpdateAsync(customer);

            return new StatusCodeReply()
            {
                Status = StatusCodes.Status200OK
            };
        }

        public override async Task<StatusCodeReply> DeleteCustomer(GuidRequest request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));

            if (customer == null)
                return new StatusCodeReply()
                {
                    Status = StatusCodes.Status404NotFound
                };

            await _customerRepository.DeleteAsync(customer);

            return new StatusCodeReply()
            {
                Status = StatusCodes.Status200OK
            };
        }
    }
}