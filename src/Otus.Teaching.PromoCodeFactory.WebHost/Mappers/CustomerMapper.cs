﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromocodeFactory.WebHost;

 namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public class CustomerMapper
    {

        public static CustomerReply ToCustomerReply(Customer customer)
        {
            var reply = new CustomerReply()
            {
                Id = customer.Id.ToString(),
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName
            };
            reply.Preferences.Add(customer.Preferences.Select(x => new PreferenceReply()
            {
                Id = x.PreferenceId.ToString(),
                Name = x.Preference.Name
            }));
            return reply;
        }
        
        public static Customer MapFromModel(CreateOrEditCustomerRequest model, IEnumerable<Preference> preferences, Customer customer = null)
        {
            if(customer == null)
            {
                customer = new Customer();
                customer.Id = Guid.NewGuid();
            }
            
            customer.FirstName = model.FirstName;
            customer.LastName = model.LastName;
            customer.Email = model.Email;

            customer.Preferences = preferences.Select(x => new CustomerPreference()
            {
                CustomerId = customer.Id,
                Preference = x,
                PreferenceId = x.Id
            }).ToList();
            
            return customer;
        }

        public static Customer MapFromCreateRequest(CreateCustomerRequest model, IEnumerable<Preference> preferences)
        {
            var customer = new Customer
            {
                Id = Guid.NewGuid(),
                FirstName = model.FirstName, 
                LastName = model.LastName,
                Email = model.Email
            };

            customer.Preferences = preferences.Select(x => new CustomerPreference()
            {
                CustomerId = customer.Id,
                Preference = x,
                PreferenceId = x.Id
            }).ToList();
            
            return customer;
        }
        
        public static Customer MapFromEditRequest(EditCustomerRequest model, IEnumerable<Preference> preferences, Customer customer)
        {
            customer.FirstName = model.FirstName;
            customer.LastName = model.LastName;
            customer.Email = model.Email;

            customer.Preferences = preferences.Select(x => new CustomerPreference()
            {
                CustomerId = customer.Id,
                Preference = x,
                PreferenceId = x.Id
            }).ToList();
            
            return customer;
        }
    }
}
