﻿using System;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.SignalR
{
    public interface ICustomerHubClient
    {
        Task ReceiveCustomerUpdate(Guid id);
    }
}