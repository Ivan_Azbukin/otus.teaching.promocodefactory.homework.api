﻿using System;
using HotChocolate;
using HotChocolate.Types;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.GraphQl
{

    public class CustomerSubscriptions
    {
        [Subscribe]
        public Customer OnEdited(
            [Topic] Guid topic,
            [EventMessage] Customer message)
        {
            return message;
        }
    }
}