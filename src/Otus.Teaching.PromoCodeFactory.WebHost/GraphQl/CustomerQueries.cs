﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HotChocolate;
using HotChocolate.Data;
using HotChocolate.Types;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.GraphQl
{
    public class CustomerQueries
    {
        [UsePaging]
        [UseFiltering]
        [UseSorting]
        public async Task<IEnumerable<Customer>> GetCustomers([Service] IRepository<Customer> customersRepository)
        {
            return await customersRepository.GetAllAsync();
        }

        public async Task<Customer> GetCustomer(Guid id,
            [Service] IRepository<Customer> customerRepository)
        {
            return await customerRepository.GetByIdAsync(id);
        }
    }
}