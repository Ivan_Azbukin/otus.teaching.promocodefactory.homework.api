﻿using System;
using System.Threading.Tasks;
using Google.Protobuf.WellKnownTypes;
using Grpc.Net.Client;
using Otus.Teaching.PromocodeFactory.WebHost;

namespace Otus.Teaching.PromoCodeFactory.GrpcClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            using var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client =  new CustomerService.CustomerServiceClient(channel);

            var allCustomersReply = await client.GetCustomersAsync(new Empty());
            foreach (var reply in allCustomersReply.Reply)
            {
                Console.WriteLine($"Customer id: {reply.Id}");
            }

            var createCustomerReply = await client.CreateCustomerAsync(new CreateCustomerRequest()
            {
                Email = "email",
                FirstName = "Name",
                LastName = "lastName",
                PreferenceIds = {"76324c47-68d2-472d-abb8-33cfa8cc0c84"}
            });
            Console.WriteLine($"Created id: {createCustomerReply.Id}");

            var editCustomerReply = await client.EditCustomerAsync(new EditCustomerRequest()
            {
                Id = createCustomerReply.Id,
                FirstName = "NewName",
                LastName = "newLastName",
                PreferenceIds = {"76324c47-68d2-472d-abb8-33cfa8cc0c84"}
            });
            Console.WriteLine($"Edit result: {editCustomerReply.Status}");

            var deleteCustomerReply = await client.DeleteCustomerAsync(new GuidRequest()
            {
                Id = createCustomerReply.Id
            });
            Console.WriteLine($"DeleteResult: {deleteCustomerReply.Status}");

        }
    }
}